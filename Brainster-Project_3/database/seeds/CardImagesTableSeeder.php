<?php

use App\Card_Image;
use Illuminate\Database\Seeder;

class CardImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cards_images = [
            1 => ['brainster_1.jpg', 'brainster_2.jpg', 'brainster_3.jpg', 'brainster_4.jpg',
            'brainster_5.jpeg', 'brainster_6.jpg', 'brainster_7.png', 'brainster_8.png'],

            2 => ['sportM_1.jpg', 'sportM_2.jpg', 'sportM_3.jpg', 'sportM_4.jpeg', 
            'sportM_5.jpg', 'sportM_6.jpg'],

            3 => ['soulKitchen_1.jpg', 'soulKitchen_2.jpg', 'soulKitchen_3.jpg', 
            'soulKitchen_4.jpg', 'soulKitchen_5.jpg', 'soulKitchen_6.jpg', 'soulKitchen_7.jpg'],

            4 => ['synergyFitness_1.jpg', 'synergyFitness_2.jpg', 'synergyFitness_3.jpg', 
            'synergyFitness_4.jpg', 'synergyFitness_5.jpg', 'synergyFitness_6.jpg', 
            'synergyFitness_7.jpeg', 'synergyFitness_8.jpg'],

            5 => ['new-yorker_1.jpg', 'new-yorker_2.jpeg', 'new-yorker_3.jpeg', 'new-yorker_4.jpg', 
            'new-yorker_5.jpg', 'new-yorker_6.jpg'],

            6 => ['grand-garden_1.jpg', 'grand-garden_2.jpg', 'grand-garden_3.jpg', 'grand-garden_4.jpg', 
            'grand-garden_5.jpg', 'grand-garden_6.jpg', 'grand-garden_7.jpg', 'grand-garden_8.jpg'],

            7 => ['fitness-Stanica_1.jpg', 'fitness-Stanica_2.jpg', 'fitness-Stanica_3.jpg', 
            'fitness-Stanica_4.jpg', 'fitness-Stanica_5.jpg', 'fitness-Stanica_6.jpg'],

            8 => ['neptun_1.jpg', 'neptun_2.jpg', 'neptun_3.jpg', 'neptun_4.jpg', 
            'neptun_5.png', 'neptun_6.jpg', 'neptun_7.jpg'],
            
        ];

        foreach($cards_images as $key => $card_imagesArr) {
            foreach($card_imagesArr as $image) {
                $card_image = new Card_Image();
                $card_image->image = $image;
                $card_image->card_id = $key;
                $card_image->save();
            }
        }

    }
}
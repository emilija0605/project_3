<?php

use Faker\Factory;
use App\Purchased_Card;
use Illuminate\Database\Seeder;

class Purchased_CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<5;$i++) {
            $purchased_card = new Purchased_Card();
            $faker = Faker\Factory::create();
            $purchased_card->full_name = $faker->name;
            $purchased_card->company_name = $faker->company;
            $purchased_card->number_of_employees = $faker->numberBetween(0, 200);
            $purchased_card->contact_number = $faker->e164PhoneNumber;
            $purchased_card->save();
        }
    }
}

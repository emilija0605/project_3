<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(Purchased_CardsTableSeeder::class);
        $this->call(CardsTableSeeder::class);
        $this->call(CardImagesTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
    }
}

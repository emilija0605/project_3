<?php

use App\Card;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $cards = [
            [
                'company_name' => 'Brainster', 'type_of_discount' => '50%', 'category_id' => 6,
                'logo' => 'brainster-logo.jpeg', 'description' => $faker->text(600), 
                'website' => 'https://brainster.co/', 'facebook_page' => 'https://www.facebook.com/brainster.co',
                'phone_number' => '+38970384728', 'email' => 'contact@brainster.co',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.9781965295147!2d21.41576191544493!3d42.00074347921261!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13541449c116c037%3A0x88831c22d3d17fb2!2sBrainster!5e0!3m2!1smk!2smk!4v1566048701800!5m2!1smk!2smk',
                'address' => 'ул. Костурски Херои 28', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'sport M', 'type_of_discount' => '30%', 'category_id' => 1,
                'logo' => 'sportM-logo.png', 'description' => $faker->text(600), 
                'website' => 'https://www.sport-m.com.mk/',
                'facebook_page' => 'https://www.facebook.com/SportMMacedonia/',
                'phone_number' => '+38923078801', 'email' => 'ONLINESHOP@SPORT-M.COM.MK',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.7983669326572!2d21.38937481544509!3d42.00460267921236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135415bc1ba04e81%3A0x4b977b0f665eaf6a!2sSport+M+Adidas+Shop!5e0!3m2!1smk!2smk!4v1566051513218!5m2!1smk!2smk',
                'address' => 'ул. Љубљанска бр.4', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'Соул Кичен Бар', 'type_of_discount' => '20%', 'category_id' => 3,
                'logo' => 'soul-kitchen-bar-logo.png', 'description' => $faker->text(600), 
                'website' => 'https://www.restorani.com.mk/restoran/soul-kichen-bar-skopje',
                'facebook_page' => 'https://www.facebook.com/SoulKitchenSoulFood/',
                'phone_number' => '+38978887733', 'email' => 'mojotemail@gmail.com',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2965.223264455869!2d21.43209621544478!3d41.995483779213004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135415b6ae1e585b%3A0x2db161df8f76b7dd!2sSoul+Kitchen+Bar+-+Balkan+Wine+%26+Cuisine!5e0!3m2!1smk!2smk!4v1566051847838!5m2!1smk!2smk',
                'address' => 'Кеј 13-ти Ноември бр.8', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'Synergy Fitness & Spa', 'type_of_discount' => '40%', 'category_id' => 4,
                'logo' => 'synergyfitness-logo.jpg', 'description' => $faker->text(600), 
                'website' => 'https://synergyfitness.com.mk/',
                'facebook_page' => 'https://www.facebook.com/SynergyFitnessandSpa/',
                'phone_number' => '+38922440005', 'email' => 'info@synergyfitness.com.mk',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2965.406239542947!2d21.449980315444694!3d41.991556379213314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13541595f048cdab%3A0x8a2a3fc385a153fa!2sSynergy+-+Fitness+%26+Spa!5e0!3m2!1sen!2smk!4v1566052283294!5m2!1sen!2smk',
                'address' => 'ул. Владимир Комаров бр. 11а', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'New Yorker', 'type_of_discount' => '70%', 'category_id' => 1,
                'logo' => 'newYorker-logo.png', 'description' => $faker->text(600), 
                'website' => 'https://www.newyorker.de/',
                'facebook_page' => 'https://www.facebook.com/pg/NewYorkerSkopje/about/?ref=page_internal',
                'phone_number' => '07Х/ХХХ-ХХХ', 'email' => 'mojotemail@gmail.com',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.789029044848!2d21.38890495055791!3d42.004803065353414!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13541471ff8e2bed%3A0x6e961704302b7994!2z0IrRg9GY0L7RgNC60LXRgA!5e0!3m2!1smk!2smk!4v1566137419942!5m2!1smk!2smk',
                'address' => 'ул. Љубљанска бр.4', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'Ресторан Гранд Гарден', 'type_of_discount' => '30%', 'category_id' => 3,
                'logo' => 'grand-garden-logo.png', 'description' => $faker->text(600), 
                'website' => 'https://www.restorani.com.mk/restoran/restoran-grand-garden-skopje',
                'facebook_page' => 'https://www.facebook.com/pg/Restoran-Grand-Garden-173959282945162/about/?ref=page_internal',
                'phone_number' => '+38975443964', 'email' => 'mojotemail@gmail.com',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.9449594490934!2d21.493816515444948!3d42.00145677921247!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x13543fcbb572c4df%3A0x9d7af3a2589125c3!2sRestaurant+Grand+Garden!5e0!3m2!1smk!2smk!4v1566052674792!5m2!1smk!2smk',
                'address' => 'ul. Palmiro Toljati br. 103', 'admin_approved' => 1,
            ],

            [
                'company_name' => 'Фитнес Станица', 'type_of_discount' => '20%', 'category_id' => 4,
                'logo' => 'fitness-Stanica-logo.png', 'description' => $faker->text(600), 
                'website' => 'http://www.fitnesstanica.mk/',
                'facebook_page' => 'https://www.facebook.com/fitnesstanica',
                'phone_number' => '+38978294486', 'email' => 'fitnesstanica@gmail.com',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2965.429626401822!2d21.436498415444625!3d41.991054379213374!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x135415b7ee3e4db7%3A0x744b1a73c633ccf0!2sFitnes+Stanica+-+Olimpiski!5e0!3m2!1smk!2smk!4v1566052796193!5m2!1smk!2smk',
                'address' => 'Бул.Кочо Рацин бр.33', 'admin_approved' => 0,
            ],

            [
                'company_name' => 'Neptun', 'type_of_discount' => '30%', 'category_id' => 1,
                'logo' => 'neptun-logo.png', 'description' => $faker->text(600), 
                'website' => 'https://www.neptun.mk/',
                'facebook_page' => 'https://www.facebook.com/NeptunMakedonija/',
                'phone_number' => '+38925514777', 'email' => 'webinfo@neptun.mk',
                'google_map_address' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2964.7827374502476!2d21.390121015445068!3d42.004938079212145!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1354146e19641625%3A0x243fa4a51ee3e447!2sNeptun+City+Mall!5e0!3m2!1smk!2smk!4v1566052876350!5m2!1smk!2smk',
                'address' => 'Манапо', 'admin_approved' => 0,
            ],

        ];

        foreach($cards as $card) {
            $card1 = new Card();
            $card1->company_name = $card['company_name'];
            $card1->type_of_discount = $card['type_of_discount'];
            $card1->category_id = $card['category_id'];
            $card1->logo = $card['logo'];
            $card1->description = $card['description'];
            $card1->website = $card['website'];
            $card1->facebook_page = $card['facebook_page'];
            $card1->phone_number = $card['phone_number'];
            $card1->email = $card['email'];
            $card1->google_map_address = $card['google_map_address'];
            $card1->address = $card['address'];
            $card1->admin_approved = $card['admin_approved'];
            $card1->save();
        }
        
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = ['logo', 'company_name', 'type_of_discount', 'category_id', 'description',
    'website', 'facebook_page', 'phone_number', 'email', 'google_map_address', 'address'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card_Image extends Model
{
    protected $fillable = ['image'];    
}

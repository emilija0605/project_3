<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuyCardValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameAndSurname' => 'required',
            'companyName' => 'required',
            'numberOfEmployees' => 'required|numeric',
            'contactNumber' => 'required|regex:/^[+]?\d+$/',
        ];
    }

    public function messages() 
    {
        return [
            'contactNumber.regex' => 'The contact number field can only contain numbers 
            and the + sign at the beginning',
        ];
    }
}

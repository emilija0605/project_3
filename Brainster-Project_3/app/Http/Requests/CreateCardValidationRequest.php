<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateCardValidationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'companyName' => 'required',
            'typeOfDiscount' => 'required',
            'category' => 'required',
            'image' => 'required',
            'shortDescription' => 'required',
            'website' => 'required',
            'facebookPage' => 'required',
            'phoneNumber' => 'required|regex:/^[+]?\d+$/',
            'email' => 'required|email',
            'googleMapsAddress' => 'required',
            'address' => 'required',
            'images' => 'required',
        ];
    }
    public function messages() 
    {
        return [
            'phoneNumber.regex' => 'The contact number field can only contain numbers 
            and the + sign at the beginning',
            'images.required' => 'This image field is required. You can add min:1 image and max:8 images.'
        ];
    }
}
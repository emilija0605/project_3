<?php

namespace App\Http\Controllers;

use App\Card;
use App\Category;
use App\Card_Image;
use Illuminate\Http\Request;

class Route_Controller extends Controller
{
    public function showHomePage() 
    {
        $page_title = "Home Page";
        $categories = Category::all();
        $cards = Card::where('admin_approved', 1)->get();
        $category_value = '';
        $cardData = [];

        foreach($cards as $card) {
            $categoryResults = Category::where('id', $card['category_id'])->get();
            foreach($categoryResults as $categoryResult) {
                $category_value = $categoryResult['category'];
            }

            $cardData[] = [
                'logo' => $card->logo, 'company_name' => $card->company_name, 
                'type_of_discount' => $card->type_of_discount, 
                'category' => $category_value, 'card_id' => $card->id,
            ];
        }

        return view('index', compact('page_title', 'categories', 'cardData'));
    }

    public function showBuyCardPage()
    {
        $page_title = "Buy a Card";
        return view('cards.buy_card', compact('page_title'));
    }

    public function showCreateCardPage()
    {
        $page_title = "Create a Discount";
        $categories = Category::all();
        return view('cards.create_card', compact('page_title', 'categories'));
    }

    public function showCardPage($id)
    {
        $page_title = "Discount Details";
        $card = Card::findOrFail($id);
        $logo = $card['logo'];
        $category_value = '';

        $categoryResults = Category::where('id', $card['category_id'])->get();
        foreach($categoryResults as $categoryResult) {
            $category_value = $categoryResult['category'];
        }
        
        $card_images = Card_Image::where('card_id', $id)->get();
        
        return view('cards.card', compact('page_title', 'card', 'logo', 'category_value', 'card_images'));
    }

}
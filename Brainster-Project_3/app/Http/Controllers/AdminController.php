<?php

namespace App\Http\Controllers;

use App\Card;
use App\Category;
use App\Card_Image;
use App\Purchased_Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\AdminLoginValidationRequest;
use App\Http\Requests\CreateCardValidationRequest;

class AdminController extends Controller
{
    public function showAdminLoginPage()
    {
        $page_title = "Admin Login";
        if (Session::has('email') && Session::get('email') == 'admin@gmail.com')
        {
            return redirect('admin-page');
        }
        return view('admin_layouts.admin_login', compact('page_title'));
    }

    public function showAdminPage()
    {
        if (Session::has('email') && Session::get('email') == 'admin@gmail.com')
        {
            $page_title = 'Admin Page';
            $purchased_cards = Purchased_Card::all();
            $new_cards = Card::where('admin_approved', 0)->get();
            return view('admin_layouts.admin_page', compact('page_title', 'purchased_cards', 'new_cards'));
        }
        else {
            return redirect('home');
        } 
    }

    public function login(AdminLoginValidationRequest $request) 
    {
        if ($request->email == 'admin@gmail.com' && $request->password == 'admin123')
        {
            session()->put(['email' => "$request->email"]);
            return redirect('admin-page');
        }
        else {
            return redirect('admin')->with('error', 'Wrong email or password, please try again!');
        }
    }

    public function logout()
    {
        session()->forget('email');
        return redirect('admin');
    }

    public function approve($id)
    {
        Card::where('id', $id)->update(['admin_approved' => 1]);
        return redirect('home')->with('message', 'Тhe new card has been successfully approved!');
    }

    public function delete($id)
    {
        $card = Card::findOrFail($id);
        $card_images = Card_Image::where('card_id', $id)->get();
        foreach($card_images as $card_image){
            $image = Card_Image::findOrFail($card_image['id']);
            $image->delete();
        }
        $card->delete();

        return redirect('home')->with('message', 'Тhe card has been successfully deleted!');
    }

    public function edit($id)
    {
        $edit_card = Card::findOrFail($id);
        $edit_card_images = Card_Image::where('card_id', $id)->get();
        $categories = Category::all();
        $page_title = "Edit Card";
      
        return view('cards.create_card', compact('page_title', 'edit_card', 'categories', 'edit_card_images'));
    }

    public function editPost(CreateCardValidationRequest $request)
    {
        if($request->edit_card_id != '') {
            $edit_card = Card::findOrFail($request->edit_card_id);
            $temp1 = $_FILES["image"]["tmp_name"];
            $imageName = $_FILES["image"]["name"];
            if(move_uploaded_file($temp1,"img/".$imageName)) {
                $edit_card->update(['logo' => $imageName]);
            }

            $edit_card_images = Card_Image::where('card_id', $request->edit_card_id)->get();
            foreach($edit_card_images as $edit_card_image){
                $edit_card_image->delete();
            }
            foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name){
                $temp = $_FILES["images"]["tmp_name"][$key];
                $name = $_FILES["images"]["name"][$key];
                if(move_uploaded_file($temp,"img/".$name)) {
                    $new_card_images = new Card_Image();
                    $new_card_images->image = $name;
                    $new_card_images->card_id = $request->edit_card_id;
                    $new_card_images->save();
                }
            }

            $edit_card->update(['company_name' => $request->companyName, 
            'type_of_discount' => $request->typeOfDiscount, 'category_id' => $request->category,
            'description' => $request->shortDescription, 'website' => $request->website, 
            'facebook_page' => $request->facebookPage, 'phone_number' => $request->phoneNumber,
            'email' => $request->email, 'google_map_address' => $request->googleMapsAddress,
            'address' => $request->address]);

            return redirect('home')->with('message', 'Тhe card has been successfully edited!');
        }
    }

}
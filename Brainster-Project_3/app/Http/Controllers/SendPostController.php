<?php

namespace App\Http\Controllers;

use App\Card;
use App\Card_Image;
use App\Purchased_Card;
use Illuminate\Http\Request;
use App\Http\Requests\BuyCardValidationRequest;
use App\Http\Requests\CreateCardValidationRequest;

class SendPostController extends Controller
{
    public function sendBuyCardPost(BuyCardValidationRequest $request)
    {
        $new_purchased_card = new Purchased_Card();
        $new_purchased_card->full_name = $request->nameAndSurname;
        $new_purchased_card->company_name = $request->companyName;
        $new_purchased_card->number_of_employees = $request->numberOfEmployees;
        $new_purchased_card->contact_number = $request->contactNumber;
        $new_purchased_card->save();

        return redirect('buy-card')->with('message', 'Your data was successfully added!');
    }

    public function sendCreateCardPost(CreateCardValidationRequest $request)
    {
        $new_card = new Card();
        $new_card->company_name = $request->companyName;
        $new_card->type_of_discount = $request->typeOfDiscount;
        $new_card->category_id = $request->category;

        $temp1 = $_FILES["image"]["tmp_name"];
        $imageName = $_FILES["image"]["name"];
        if(move_uploaded_file($temp1,"img/".$imageName)) {
            $new_card->logo = $imageName;
        }
        
        $new_card->description = $request->shortDescription;
        $new_card->website = $request->website;
        $new_card->facebook_page = $request->facebookPage;
        $new_card->phone_number = $request->phoneNumber;
        $new_card->email = $request->email;
        $new_card->google_map_address = $request->googleMapsAddress;
        $new_card->address = $request->address;
        $new_card->save();

        $id = $new_card->id;
        
        foreach($_FILES["images"]["tmp_name"] as $key=>$tmp_name){
            $temp = $_FILES["images"]["tmp_name"][$key];
            $name = $_FILES["images"]["name"][$key];
            if(move_uploaded_file($temp,"img/".$name)) {
                $new_card_images = new Card_Image();
                $new_card_images->image = $name;
                $new_card_images->card_id = $id;
                $new_card_images->save();
            }
        }

        return redirect('create-card')->with('message', 'New card was successfully created!');
    }

}
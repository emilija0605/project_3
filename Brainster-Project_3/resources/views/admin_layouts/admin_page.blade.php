@extends('layouts.master')

@section('content')
<div class="container-fluid admin1">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 flex1">
            <span class="title-h">Welcome Admin!</span>
            <a href="{{ route('logout') }}" role="button" class="btn btn-default loginBtn">LOG OUT</a>
        </div>
    </div>
</div>
<div class="container-fluid admin2">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 flex1">
            <button type="button" id="showPurchasedCards" class="btn btn-default showBtn">Show purchased cards</button>
            <button type="button" id="showCreatedCards" class="btn btn-default showBtn">Show created cards</button>
        </div>
    </div>
</div>
<div class="container-fluid" id="purchasedCards">
    <div class="row">
        <div class="table-responsive col-md-10 col-md-offset-1">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Реден број</th>
                        <th>Име и презиме</th>
                        <th>Компанија</th>
                        <th>Број на вработени</th>
                        <th>Телефон за контакт</th>
                        <th>Дата на внесување на апликацијата</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($purchased_cards as $purchased_card)
                    <tr>
                        <td>{{ $purchased_card['id'] }}</td>
                        <td>{{ $purchased_card['full_name'] }}</td>
                        <td>{{ $purchased_card['company_name'] }}</td>
                        <td>{{ $purchased_card['number_of_employees'] }}</td>
                        <td>{{ $purchased_card['contact_number'] }}</td>
                        <td>{{ $purchased_card['created_at'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container-fluid" id="createdCards">
    <div class="row">
        <div class="table-responsive col-md-10 col-md-offset-1">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Реден број</th>
                        <th>Компании кои нудат попуст</th>
                        <th>Разгледај го попустот</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($new_cards as $new_card)
                    <tr>
                        <td>{{ $new_card['id'] }}</td>
                        <td>{{ $new_card['company_name'] }}</td>
                        <td><a href="{{ route('card', ['id' => $new_card['id']]) }}" class="seeMoreLink">Види повеќе</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
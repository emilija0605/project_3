@extends('layouts.master')

@section('content')
<form method="POST" action="{{ route('admin.post') }}">
    @csrf
    <div class="container-fluid admin-box">
        @if(session()->has('error'))
        <div class="alert alert-danger">
            <p>{{ session()->get('error') }}</p>
        </div>
        @endif
        <div class="row form1">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 form-group">
                @error('email')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="email">Емаил</label>
                <input type="text" class="form-control changed-color" id="email" name="email"
                    placeholder="пр. mojotemail@gmail.com" value="{{ old('email') }}">
            </div>
        </div>
        <div class="row form2">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 form-group">
                @error('password')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="password">Пасворд</label>
                <input type="password" class="form-control changed-color" id="password" name="password" value="">
            </div>
        </div>
        <div class="row form3">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 send-top">
                <button type="submit" class="btn btn-default loginBtn">LOGIN</button>
            </div>
        </div>
    </div>
</form>
@stop
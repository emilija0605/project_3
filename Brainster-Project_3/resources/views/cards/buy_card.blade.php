@extends('layouts.master')

@section('content')
<div class="container-fluid box-1">
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary backToHomeBtn" href="{{ route('home') }}" role="button"><i
                    class="fas fa-arrow-left"></i></a>
        </div>
    </div>
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <h1 class="title-h">Купи картичка за вработените</h1>
        </div>
    </div>
</div>

<div class="container-fluid box-2">
    @if(session()->has('message'))
    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
        <p class="text-success">
            <strong>{{ session()->get('message') }}</strong>
        </p>
    </div>
    @endif
    <form method="POST" action="{{ route('buy-card.post') }}" class="p0">
        @csrf
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('nameAndSurname')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="name&surname">Име и презиме</label>
                <input type="text" class="form-control changed-color" id="name&surname" name="nameAndSurname"
                    placeholder="пр. Кристијан Ѓорѓиевски" value="{{ old('nameAndSurname') }}">
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('companyName')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="company_name">Име на компанија</label>
                <input type="text" class="form-control changed-color" id="company_name" name="companyName"
                    placeholder="пр. Мајкрософт ДООЕЛ" value="{{ old('companyName') }}">
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-5 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('numberOfEmployees')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="number_of_employees">Број на вработени</label>
                <input type="text" class="form-control changed-color" id="number_of_employees" name="numberOfEmployees"
                    placeholder="пр. 200" value="{{ old('numberOfEmployees') }}">
            </div>
            <div class="col-md-5 col-md-offset-0 col-sm-10 col-sm-offset-1 form-group">
                @error('contactNumber')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="contact_number">Телефон за контакт</label>
                <input type="text" class="form-control changed-color" id="contact_number" name="contactNumber"
                    placeholder="пр. 07Х/ХХХ-ХХХ" value="{{ old('contactNumber') }}">
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 send-top">
                <button type="submit" class="btn btn-default buyNowBtn">ИСПРАТИ</button>
            </div>
        </div>
    </form>
</div>
@stop
@extends('layouts.master')

@section('content')
<div class="container-fluid card-background">
    <div class="row p0">
        <div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
            <div style='background-image: url({{ asset('img/'.$card["logo"]) }}); height: 80px; width: 200px;
                            background-size: contain; background-position: center; background-repeat: no-repeat;
                            margin-bottom: 10px;'>
            </div>
        </div>
    </div>
    <div class="row p0">
        <div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1">
            <button class="btn btn-default categoryBtn1 top1" type="button">{{ $category_value }}</button>
        </div>
    </div>
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <h1 class="title-h">Попуст во најдобрата компанија во градот.</h1>
            <h4 class="sale">{{ $card['type_of_discount'] }} при купување</h4>
        </div>
    </div>
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 same-height">
            <div id='carousel-custom' class='col-md-7 col-sm-6 col-xs-12 carousel slide' data-ride='carousel'>
                <div class='carousel-outer'>
                    <!-- Wrapper for slides -->
                    <div class='carousel-inner'>
                        @foreach($card_images as $key => $card_image)
                        @if($key == 0)
                        <div class='item img-responsive active'>
                            <img src='{{ URL::to('/img/'.$card_image['image']) }}' alt='' />
                        </div>
                        @else
                        <div class='item img-responsive'>
                            <img src='{{ URL::to('/img/'.$card_image['image']) }}' alt='' />
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
                <!-- Indicators -->
                <ol class='carousel-indicators'>
                    @foreach($card_images as $key => $card_image)
                    <li data-target='#carousel-custom' data-slide-to='{{ $key }}' class='active'>
                        <img src='{{ URL::to('/img/'.$card_image['image']) }}' alt='' /></li>
                    @endforeach
                </ol>
                <div class="text">
                    {{ $card['description'] }}
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12 box-about">
                <h2 class="white">750 денари</h2>
                <div class="like-Buttons">
                    <button class="btn btn-default like-Button1" type="button"><i class="fas fa-thumbs-up"></i></button>
                    <span>209</span>
                    <button class="btn btn-default like-Button2" type="button"><i
                            class="fas fa-thumbs-down"></i></button>
                    <span>43</span>
                </div>
                <div class="text-box">
                    <h5>Имејл</h5>
                    <span class="span1">{{ $card['email'] }}</span>
                </div>
                <div class="text-box">
                    <h5>Физичка адреса</h5>
                    <span class="span1">{{ $card['address'] }}</span>
                </div>
                <div class="text-box">
                    <h5>Вебсајт</h5>
                    <span class="span1">{{ $card['website'] }}</span>
                    <button class="btn btn-default shareBtn" type="button"><i class="fas fa-share-square"></i></button>
                </div>
                <div class="text-box">
                    <h5>Фејсбук страна</h5>
                    <span class="span1">{{ $card['facebook_page'] }}</span>
                    <button class="btn btn-default shareBtn" type="button"><i class="fas fa-share-square"></i></button>
                </div>
                <div class="text-box">
                    <h5>Телефонски број</h5>
                    <span class="span1">{{ $card['phone_number'] }}</span>
                    <button class="btn btn-default shareBtn" type="button"><i class="fas fa-share-square"></i></button>
                </div>
                <div class="map-box">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <object type="text/html" data="{{ $card['google_map_address'] }}"
                                style="height: 300px; width: 100%; border: 0; overflow: hidden;" id="cc-embed"></object>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::has('email'))
    <div clas="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 adminButtons">
            <a href="{{ route('admin.approve', ['id' => $card['id']]) }}" class="btn btn-default adminBtn1"
                role="button">ОДОБРИ</a>
            <a href="{{ route('admin.delete', ['id' => $card['id']]) }}" class="btn btn-default adminBtn2"
                role="button">ОДБИ</a>
            <a href="{{ route('admin.edit', ['id' => $card['id']]) }}" class="btn btn-default adminBtn3"
                role="button">ЕДИТИРАЈ</a>
        </div>
    </div>
    @endif
</div>
@stop
@extends('layouts.master')

@section('content')
<div class="container-fluid box-1">
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <a class="btn btn-primary backToHomeBtn" href="{{ route('home') }}" role="button"><i
                    class="fas fa-arrow-left"></i></a>
        </div>
    </div>
    <div class="row p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
            <h1 class="title-h">Понудете картичка со попуст</h1>
        </div>
    </div>
</div>

<div class="container-fluid box-3">
    @if(session()->has('message'))
    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
        <p class="text-success">
            <strong>{{ session()->get('message') }}</strong>
        </p>
    </div>
    @endif
    <form method="POST" enctype="multipart/form-data" @if($page_title=="Create a Discount" )
        action="{{ route('create-card.post') }}" @else action="{{ route('admin.edit.post') }}" @endif class="p0">
        @csrf
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('companyName')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="company_name">Име на компанијата која нуди попуст</label>
                <input type="text" class="form-control changed-color" id="company_name" name="companyName"
                    placeholder="пр. Мајкрософт ДООЕЛ" @if(!empty($edit_card)) value="{{ $edit_card['company_name'] }}"
                    @else value="{{ old('companyName') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('typeOfDiscount')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="type_of_discount">Вид на попуст</label>
                <input type="text" class="form-control changed-color" id="type_of_discount" name="typeOfDiscount"
                    placeholder="пр. 50% на купување" @if(!empty($edit_card))
                    value="{{ $edit_card['type_of_discount'] }}" @else value="{{ old('typeOfDiscount') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                @error('category')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="select_category">Избери категорија</label>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                <div data-toggle="buttons" id="select_category">
                    @foreach($categories as $category)
                    <span class="btn btn-default">
                        <input type="radio" name="category" value="{{ $category->id }}">{{ $category->category }}</span>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                @error('image')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="thumbnail">Постави thumbnail</label>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group" id="thumbnail">
                <div class="wrap-custom-file">
                    <input type="file" name="image" id="image" accept=".png" />
                    <label for="image">
                        <i class="fa fa-plus"></i>
                    </label>
                </div>
                <h6 class="png-type">Сликата мора да биде во .png формат</h6>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('shortDescription')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="short_description">Опис на понудата</label>
                <textarea class="form-control changed-color" rows="5" placeholder="Краток опис на вашата понуда"
                    name="shortDescription" id="short_description">@if(!empty($edit_card)){{ $edit_card['description'] }} @else {{ old('shortDescription') }}@endif
                </textarea>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('website')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="website">Линк до вашиот вебсајт</label>
                <input type="text" class="form-control changed-color" id="website" name="website"
                    placeholder="пр. https://mojotvebsajt.com" @if(!empty($edit_card))
                    value="{{ $edit_card['website'] }}" @else value="{{ old('website') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('facebookPage')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="facebook_page">Фејсбук страна</label>
                <input type="text" class="form-control changed-color" id="facebook_page" name="facebookPage"
                    placeholder="пр. https://facebook.com/my_company" @if(!empty($edit_card))
                    value="{{ $edit_card['facebook_page'] }}" @else value="{{ old('facebookPage') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('phoneNumber')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="phone_number">Телефонски број</label>
                <input type="text" class="form-control changed-color" id="phone_number" name="phoneNumber"
                    placeholder="пр. 07Х/ХХХ-ХХХ" @if(!empty($edit_card)) value="{{ $edit_card['phone_number'] }}" @else
                    value="{{ old('phoneNumber') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('email')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="email">Вашиот имејл</label>
                <input type="text" class="form-control changed-color" id="email" name="email"
                    placeholder="пр. mojotemail@gmail.com" @if(!empty($edit_card)) value="{{ $edit_card['email'] }}"
                    @else value="{{ old('email') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('googleMapsAddress')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="google_maps_address">Google maps адреса</label>
                <input type="text" class="form-control changed-color" id="google_maps_address" name="googleMapsAddress"
                    placeholder="пр. https://www.google.com/maps/place/your+place" @if(!empty($edit_card))
                    value="{{ $edit_card['google_map_address'] }}" @else value="{{ old('googleMapsAddress') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 form-group">
                @error('address')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="address">Физичка адреса</label>
                <input type="text" class="form-control changed-color" id="address" name="address"
                    placeholder="пр. Партизански херои бб." @if(!empty($edit_card)) value="{{ $edit_card['address'] }}"
                    @else value="{{ old('address') }}" @endif>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
                @error('images')
                <p class="text-danger">
                    <strong>{{ $message }}</strong>
                </p>
                @enderror
                <label for="images">Фотографии</label>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1" id="images">
                <div class="wrap-custom-file web mobile">
                    <input type="file" name="images[]" id="image1" accept=".jpg, .jpeg, .png" />
                    <label for="image1">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file web">
                    <input type="file" name="images[]" id="image2" accept=".jpg, .jpeg, .png" />
                    <label for="image2">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file web mobile">
                    <input type="file" name="images[]" id="image3" accept=".jpg, .jpeg, .png" />
                    <label for="image3">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file">
                    <input type="file" name="images[]" id="image4" accept=".jpg, .jpeg, .png" />
                    <label for="image4">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file web mobile">
                    <input type="file" name="images[]" id="image5" accept=".jpg, .jpeg, .png" />
                    <label for="image5">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file web">
                    <input type="file" name="images[]" id="image6" accept=".jpg, .jpeg, .png" />
                    <label for="image6">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file web mobile">
                    <input type="file" name="images[]" id="image7" accept=".jpg, .jpeg, .png" />
                    <label for="image7">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
                <div class="wrap-custom-file">
                    <input type="file" name="images[]" id="image8" accept=".jpg, .jpeg, .png" />
                    <label for="image8">
                        <i class="fa fa-image"></i>
                    </label>
                </div>
            </div>
        </div>
        <div class="row p0">
            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 send-top">
                <input type="hidden" name="edit_card_id" @if(!empty($edit_card)) value="{{ $edit_card['id'] }}" @else
                    value="" @endif>
                <button type="submit" class="btn btn-default buyNowBtn">ИСПРАТИ</button>
            </div>
        </div>
    </form>
</div>
@stop
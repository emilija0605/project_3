@extends('layouts.master')

@section('content')
<div class="container-fluid header">
    <div class="row p0">
        <h1 class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 title-h">Купете
            картичка со попуст за
            вашите
            вработени.</h1>
    </div>
    <div class="row p0">
        <p class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 subtitle-h">Најдобри онлајн попусти
            за
            производи, услуги, фитнес центри,
            ресторани, едукација и кариера.</p>
    </div>
    <div class="row p0">
        <p class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12"><a class="btn btn-primary buyNowBtn"
                href="{{ route('buy-card') }}" role="button">КУПИ
                СЕГА</a></p>
    </div>
</div>

<div class="container-fluid search">
    <div class="row p0">
        <form>
            <div class="col-md-5 col-md-offset-1 col-sm-5 col-sm-offset-1">
                <input type="search" class="form-control" id="search" name="search" placeholder="Пребарај попусти">
            </div>
            <div class="col-md-3 col-sm-3 select-box-paddings">
                <div class="select-box">
                    <div class="select-box__current" tabindex="1">
                        @foreach($categories as $category)
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio" id="{{$category->id}}"
                                value="{{$category->id}}" name="select" checked="checked" />
                            <p class="select-box__input-text">{{$category->category}}</p>
                        </div>
                        @endforeach
                        <img class="select-box__icon" src="http://cdn.onlinewebfonts.com/svg/img_295694.svg"
                            alt="Arrow Icon" aria-hidden="true" />
                        <div class="select-box__value">
                            <input class="select-box__input" type="radio" id="0" value="" name="select"
                                checked="checked" />
                            <p class="select-box__input-text">Сите</p>
                        </div>
                    </div>
                    <ul class="select-box__list">
                        <li>
                            <label class="select-box__option borders-top" for="0" aria-hidden="aria-hidden">Сите</label>
                        </li>
                        @foreach($categories as $category)
                        @if($category->id === 6)
                        <li>
                            <label class="select-box__option borders-bottom" for="{{$category->id}}"
                                aria-hidden="aria-hidden">{{$category->category}}</label>
                        </li>
                        @else
                        <li>
                            <label class="select-box__option" for="{{$category->id}}"
                                aria-hidden="aria-hidden">{{$category->category}}</label>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-2 hidden-xs buttons">
                <button class="btn btn-default showGridBtn" id="showGrid" type="button"><i
                        class="fas fa-th"></i></button>
                <button class="btn btn-default showSingleCardBtn" id="showSingleCards" type="button"><i
                        class="fas fa-th-list"></i></button>
            </div>
        </form>
    </div>
</div>

<div id="grid" class="container-fluid cards-box">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 wrap">
            @if(session()->has('message'))
            <div class="alert alert-success" style="margin-top: 15px;">
                <p>{{ session()->get('message') }}</p>
            </div>
            @endif
            @foreach($cardData as $card)
            <div data-target="{{ $card['card_id'] }}" class="col-md-4 col-sm-4 col-xs-12 paddings">
                <div class="card text-center">
                    <div class="flex">
                        <div style='background-image: url({{ asset('img/'.$card["logo"]) }}); height: 80px; width: 200px;
                                background-size: contain; background-position: center; background-repeat: no-repeat;
                                margin-bottom: 10px;'>
                        </div>
                    </div>
                    <h4 class="company-name">{{ $card['company_name'] }}</h4>
                    <h5 class="discount">{{ $card['type_of_discount'] }} попуст во првите 6 месеци</h5>
                    <div>
                        <button class="btn btn-default categoryBtn top1" type="button">{{ $card['category'] }}</button>
                    </div>
                    <div class="likes margins1">
                        <button class="btn btn-default likeBtn" type="button"><i class="fas fa-thumbs-up"></i></button>
                        <span>142</span>
                        <button class="btn btn-default dislikeBtn" type="button"><i
                                class="fas fa-thumbs-down"></i></button>
                    </div>
                    <div>
                        <a href="{{ route('card', ['id' => $card['card_id']]) }}" target="_blank"
                            class="btn btn-default seeMoreBtn bottom1" role="button">ВИДИ ПОВЕЌЕ</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div id="single-cards" class="container-fluid cards-box">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 hidden-xs">
            @foreach($cardData as $card)
            <div data-target="{{ $card['card_id'] }}" class="col-md-12 col-sm-12 paddings">
                <div class="col-md-12 col-sm-12 card flex">
                    <div class="col-md-3 col-sm-3 flex">
                        <div style='background-image: url({{ asset('img/'.$card["logo"]) }}); height: 80px; width: 200px;
                            background-size: contain; background-position: center; background-repeat: no-repeat;'>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-5">
                        <h4 class="company-name1">{{ $card['company_name'] }}</h4>
                        <h5 class="discount">{{ $card['type_of_discount'] }} попуст во првите 6 месеци</h5>
                        <div>
                            <button class="btn btn-default categoryBtn top2"
                                type="button">{{ $card['category'] }}</button>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 text-center">
                        <div>
                            <a href="{{ route('card', ['id' => $card['card_id']]) }}" target="_blank"
                                class="btn btn-default seeMoreBtn bottom2" role="button">ВИДИ
                                ПОВЕЌЕ</a>
                        </div>
                        <div class="likes margins2">
                            <button class="btn btn-default likeBtn" type="button"><i
                                    class="fas fa-thumbs-up"></i></button>
                            <span>142</span>
                            <button class="btn btn-default dislikeBtn" type="button"><i
                                    class="fas fa-thumbs-down"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="hidden-lg hidden-md hidden-sm col-xs-12">
            @foreach($cardData as $card)
            <div data-target="{{ $card['card_id'] }}" class="col-md-4 col-sm-4 paddings">
                <div class="card text-center">
                    <div class="flex">
                        <div style='background-image: url({{ asset('img/'.$card["logo"]) }}); height: 80px; width: 200px; background-size: contain; background-position: center; background-repeat: no-repeat;
                                margin-bottom: 10px;'>
                        </div>
                    </div>
                    <h4 class="company-name">{{ $card['company_name'] }}</h4>
                    <h5 class="discount">{{ $card['type_of_discount'] }} попуст во првите 6 месеци</h5>
                    <div>
                        <button class="btn btn-default categoryBtn top1" type="button">{{ $card['category'] }}</button>
                    </div>
                    <div class="likes margins1">
                        <button class="btn btn-default likeBtn" type="button"><i class="fas fa-thumbs-up"></i></button>
                        <span>142</span>
                        <button class="btn btn-default dislikeBtn" type="button"><i
                                class="fas fa-thumbs-down"></i></button>
                    </div>
                    <div>
                        <a href="{{ route('card', ['id' => $card['card_id']]) }}" target="_blank"
                            class="btn btn-default seeMoreBtn bottom1" role="button">ВИДИ ПОВЕЌЕ</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

<div class="container-fluid footer">
    <div class="row p0">
        <h1 class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 title-f">Дали сте
            заинтересирани
            вашата компанија да понуди попуст?</h1>
    </div>
    <div class="row p0">
        <p class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12 subtitle-f">Најдобри онлајн
            попусти
            за
            производи, услуги, фитнес центри,
            ресторани, едукација и кариера.</p>
    </div>
    <div class="row p0">
        <p class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12"><a
                class="btn btn-primary createDiscountBtn" href="{{ route('create-card') }}" role="button">КРЕИРАЈ
                ПОПУСТ</a>
        </p>
    </div>
</div>
@stop
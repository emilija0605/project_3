<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', 'Route_Controller@showHomePage')->name('home');

Route::get('/buy-card', 'Route_Controller@showBuyCardPage')->name('buy-card');

Route::get('/create-card', 'Route_Controller@showCreateCardPage')->name('create-card');

Route::get('/card/{id}', 'Route_Controller@showCardPage')->name('card');

Route::get('/admin', 'AdminController@showAdminLoginPage')->name('admin');

Route::get('/admin-page', 'AdminController@showAdminPage')->name('admin-page');

Route::get('/logout', 'AdminController@logout')->name('logout');

Route::get('/admin/approve/{id}', 'AdminController@approve')->name('admin.approve');

Route::get('/admin/delete/{id}', 'AdminController@delete')->name('admin.delete');

Route::get('/admin/edit/{id}', 'AdminController@edit')->name('admin.edit');

Route::get('/home/search','SearchController@liveSearch')->name('home.search');

Route::get('/home/select','SearchController@selectSearch')->name('home.select');


Route::post('/buy-card/post', 'SendPostController@sendBuyCardPost')->name('buy-card.post');

Route::post('/create-card/post', 'SendPostController@sendCreateCardPost')->name('create-card.post');

Route::post('/admin/post', 'AdminController@login')->name('admin.post');

Route::post('/admin/edit/post', 'AdminController@editPost')->name('admin.edit.post');


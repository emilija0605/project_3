$(function () {
    /* Home Page - show grid and single cards */

    $("#showGrid").click(function () {
        $("#grid").show();
        $("#single-cards").hide();
    });

    $("#showSingleCards").click(function () {
        $("#single-cards").show();
        $("#grid").hide();
    });

    /* Admin Page - show purchased and created cards */

    $("#showPurchasedCards").click(function () {
        $("#purchasedCards").show();
        $("#createdCards").hide();
    });

    $("#showCreatedCards").click(function () {
        $("#createdCards").show();
        $("#purchasedCards").hide();
    });

    /* Home Page - search with ajax call */

    let str1 = "";
    let str2 = "";
    let cardId = "";
    let filtered_cards = [];
    let all_cards = $('.paddings');

    $("#search").keyup(function () {
        str1 = $("#search").val();
        let radios = document.getElementsByName('select');
        for (let i = 0, length = radios.length; i < length; i++) {
            if (radios[i].checked) {
                str2 = radios[i].value;
                break;
            }
        }

        if (str1 == "" && str2 == "") {
            $.each(all_cards, function () {
                $(this).show();
            });
        } else {
            $.get({
                url: "/home/search",
                data: {
                    search: str1,
                    select: str2,
                },
                success: function (response) {
                    $.each(response, function () {
                        cardId = this['card_id'];
                        $.each(all_cards, function () {
                            if (cardId == $(this).data("target")) {
                                filtered_cards.push(this);
                            }
                        });
                    });
                    let new_arr = subtractarrays(all_cards, filtered_cards)
                    $.each(new_arr, function () {
                        $(this).hide();
                    });
                    filtered_cards.length = 0;
                    new_arr.length = 0;
                }
            });
        }
    });

    let selectVal = "";
    let searchVal = "";
    let cardId1 = "";
    let filtered_cards1 = [];

    $('input[name=select]').change(function () {
        if ($(this).is(':checked')) {
            selectVal = this.value;
            searchVal = $("#search").val();

            $.get({
                url: "/home/select",
                data: {
                    select: selectVal,
                    search: searchVal,
                },
                success: function (response) {
                    $("#results").html(response);
                    $.each(response, function () {
                        cardId1 = this['card_id'];
                        $.each(all_cards, function () {
                            if (cardId1 == $(this).data("target")) {
                                filtered_cards1.push(this);
                            }
                        });
                    });
                    let new_arr1 = subtractarrays(all_cards, filtered_cards1)
                    $.each(new_arr1, function () {
                        $(this).hide();
                    });
                    $.each(filtered_cards1, function () {
                        $(this).show();
                    });

                    filtered_cards1.length = 0;
                    new_arr1.length = 0;
                }
            });
        }
    });

    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}'
        }
    });

    function subtractarrays(array1, array2) {
        let difference = [];
        for (let i = 0; i < array1.length; i++) {
            if ($.inArray(array1[i], array2) == -1) {
                difference.push(array1[i]);
            }
        }

        return difference;
    }

});

$('input[type="file"]').each(function () {
    // Refs
    var $file = $(this),
        $label = $file.next('label'),
        $labelText = $label.find('span'),
        labelDefault = $labelText.text();

    // When a new file is selected
    $file.on('change', function (event) {
        var fileName = $file.val().split('\\').pop(),
            tmppath = URL.createObjectURL(event.target.files[0]);
        //Check successfully selection
        if (fileName) {
            $label
                .addClass('file-ok')
                .css('background-image', 'url(' + tmppath + ')');
            $labelText.text(fileName);
        } else {
            $label.removeClass('file-ok');
            $labelText.text(labelDefault);
        }
    });

    // End loop of file input elements
});

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') +
        '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();